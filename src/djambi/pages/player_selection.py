from __future__ import annotations
import pygame
import pygame.freetype
from djambi.components.components import BtnSprite, TextSprite
from djambi.game import Game


class PlayerSelection:
    def __init__(self, game: Game):
        """This module contains the PlayerSelection class. It represents the player selection page of the Djambi game.
        The player selection page is responsible for displaying the player selection page on the screen.

        Args:
            game (Game): The game object that instanciate game rules and the game loop
        """
        self.screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
        self.colorBg = (0, 0, 0)
        self.colorText = (255, 255, 255)
        self.title: TextSprite
        self.bg = pygame.image.load("./src/djambi/assets/background.jpg")
        self.btn2: BtnSprite
        self.btn4: BtnSprite
        self.game = game
        self.enable = True

    def main(self):
        """Method that displays the player selection page on the screen.
        """
        self.screen.fill(self.colorBg)
        middle = self.screen.get_width() / 2
        self.title = TextSprite(self.screen, "Players", middle, 80, self.colorText)
        self.screen.blit(self.bg, (0, 0))
        self.title.draw()
        self.btn2 = BtnSprite(self.screen, 350, 300, 200, 50, "2", 150, self.colorText, self.colorBg)
        self.btn2.draw()
        self.btn4 = BtnSprite(self.screen, 710, 300, 200, 50, "4", 150, self.colorText, self.colorBg)
        self.btn4.draw()
        pygame.display.update()

    def update(self, click: bool):
        """Method that updates the player selection page. It is called at each iteration of the game loop. It allows to update the player selection page.

        Args:
            click (bool): get the mouse click event
        """
        mouse_pos = pygame.mouse.get_pos()
        if click:
            if self.enable and self.btn2.rect.collidepoint(mouse_pos):
                print("2")
                self.game.change_page("board_view")
                # self.enable = False
            elif self.enable and self.btn4.rect.collidepoint(mouse_pos):
                self.game.change_page("board_view")
                print("4")
