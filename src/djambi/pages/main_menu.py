from __future__ import annotations
import pygame
import pygame.freetype
from djambi.components.components import BtnSprite, TextSprite
from djambi.game import Game

'''This module contains the MainMenu class. It represents the main menu of the Djambi game.
The main menu is responsible for displaying the main menu on the screen.'''


class MainMenu:
    def __init__(self, game: Game):
        """__init__ Constructor for the MainMenu class.

        Args:
            game (Game): the game object that instanciate game rules and the game loop
        """
        self.screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
        self.colorBg = (0, 0, 0)
        self.colorText = (255, 255, 255)
        self.title: TextSprite
        self.bg = pygame.image.load("./src/djambi/assets/background.jpg")
        self.start: BtnSprite
        self.quit: BtnSprite
        self.game = game
        self.enable = True

    def main(self):
        """Method that displays the main menu on the screen.
        """
        self.screen.fill(self.colorBg)
        middle = self.screen.get_width() / 2
        self.title = TextSprite(self.screen, "Djambi", middle, 80, self.colorText)
        self.screen.blit(self.bg, (0, 0))
        self.title.draw()
        self.start = BtnSprite(self.screen, 100, 200, 200, 50, "Play", 50, self.colorText, self.colorBg)
        self.start.draw()
        self.quit = BtnSprite(self.screen, 100, 300, 200, 50, "Quit", 50, self.colorText, self.colorBg)
        self.quit.draw()
        pygame.display.update()

    def update(self, click: bool):
        """Method that updates the main menu. It is called at each iteration of the game loop. It allows to update the main menu.

        Args:
            click (bool): a boolean that indicates if the mouse is clicked
        """
        mouse_pos = pygame.mouse.get_pos()
        if click:
            if self.enable and self.start.rect.collidepoint(mouse_pos):
                self.game.change_page("player_selection")
                self.enable = False

            elif self.enable and self.quit.rect.collidepoint(mouse_pos):
                pygame.quit()
                quit()
