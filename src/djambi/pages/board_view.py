import pygame
from djambi.classes.board import Board
from djambi.setup.constants import WHITE, Roles

'''This module contains the BoardView class. It represents the view of the board.
The view is responsible for displaying the board on the screen.'''


class BoardView:
    def __init__(self, screen):
        """__init__ Constructor for the BoardView class.

        Args:
            screen (Object): The screen variable of the game in Game.py file. Needed from pygame
        """
        self.screen = screen
        self.board = Board(9, 9, screen.get_width(), screen.get_height(), 2)
        self.selected_piece = None

    def first_draw(self):
        """Method that draws the board on the screen for the first time.
        """
        self.screen.fill((WHITE))  # Fill the screen with white color
        self.board.draw(self.screen)  # Draw the board on the screen
        self.board.build_board()

    def draw(self):
        """Method that draws the board on the screen.
        """
        self.screen.fill((WHITE))  # Fill the screen with white color
        self.board.draw(self.screen)  # Draw the board on the screen

        # Draw pieces on the board
        square_width = self.screen.get_width() / self.board.cols
        square_height = self.screen.get_height() / self.board.rows

        # Draw the chef icon in the middle of the board
        middle_row = self.board.rows // 2
        middle_col = self.board.cols // 2
        middle_case_x = middle_col * square_width
        middle_case_y = middle_row * square_height

        chef_icon = Roles.Chef.value(50, 0).icon  # Assuming team index 0 for the chef
        chef_icon = pygame.transform.scale(chef_icon, (int(square_width * 0.8), int(square_height * 0.8)))
        chef_icon_x = middle_case_x + (square_width - chef_icon.get_width()) / 2
        chef_icon_y = middle_case_y + (square_height - chef_icon.get_height()) / 2

        pygame.draw.rect(self.screen, pygame.Color("gray"), (middle_case_x, middle_case_y, square_width, square_height))
        self.screen.blit(chef_icon, (chef_icon_x, chef_icon_y))

    def update(self):
        """Method that updates the board view. It is called at each iteration of the game loop. It allows to update the board view.
        """
        # TODO : Implement the update method of the board view
        pass
