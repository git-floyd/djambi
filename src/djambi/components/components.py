import pygame
from pygame.sprite import Sprite
from pygame.rect import Rect

'''This module contains the components of the Djambi game. The components are responsible for displaying the game elements on the screen.'''


def create_surface_with_text(text, font_size, text_rgb, bg_rgb):
    """ Returns surface with text written on """
    font = pygame.freetype.SysFont("Courier", font_size, bold=True)
    surface, _ = font.render(text=text, fgcolor=text_rgb, bgcolor=bg_rgb)
    return surface.convert_alpha()


'''This class represents a button sprite. It inherits from the Sprite class of the Pygame library. It is responsible for displaying a button on the screen.'''


class BtnSprite(Sprite):
    def __init__(self, screen, x, y, width, height, text, text_font_size, text_rgb, bg_rgb, action=None):
        super().__init__()
        self.image = create_surface_with_text(text, text_font_size, text_rgb, bg_rgb)
        self.rect = Rect(x, y, width, height)
        self.screen = screen
        self.action = action

    '''Method that displays the button on the screen.'''

    def draw(self):
        self.screen.blit(self.image, self.rect)

    '''Method that updates the button. It is called at each iteration of the game loop. It allows to update the button.'''

    def update(self, mouse_pos):
        if self.rect.collidepoint(mouse_pos):
            if self.action == pygame.QUIT:
                pygame.quit()
            else:
                self.action()


'''This class represents a text sprite. It inherits from the Sprite class of the Pygame library. It is responsible for displaying text on the screen.'''


class TextSprite(Sprite):

    def __init__(self, screen, text, x, y, color):
        super().__init__()
        self.font = pygame.freetype.Font(None, 100)
        self.text, self.text_rect = self.font.render(text, color)  # Unpack the tuple returned by render
        self.rect = self.text.get_rect(center=(x, y))
        self.screen = screen

    '''Method that displays the text on the screen.'''

    def draw(self):
        self.screen.blit(self.text, self.rect)
