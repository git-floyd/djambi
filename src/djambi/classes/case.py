# case.py
from djambi.classes.pieces.piece import Piece

''' This module contains the Case class. It represents a case on the board of the Djambi game. A case is responsible for managing the cases of the board.'''


class Case:
    def __init__(self, x, y):
        """Initializes the case with its position on the board.

        Args:
            x (_type_): float: position on the x-axis
            y (_type_): float: position on the y-axis
        """
        self.x = x
        self.y = y
        self.piece = None

    @property
    def position(self):
        """Returns the position of the case as a tuple (x, y)

        Returns:
            _type_: tuple (float, float): the position of the case
        """
        return (self.x, self.y)

    def is_occupied(self):
        """Returns True if the case is occupied by a piece, False otherwise.

        Returns:
            _type_: piece (Piece): the piece positionned on the tile
        """
        return self.piece is not None

    def set_piece(self, piece: Piece):
        """Sets the piece on the case.

        Args:
            piece (Piece): the piece positionned on the tile

        Returns:
            _type_: _description_
        """
        self.piece = piece
        return self
