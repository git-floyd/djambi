from djambi.classes.pieces.piece import Piece
from djambi.team import Team
import pygame


class Reporter(Piece):
    def __init__(self, square_size, teamName):
        """__init__ Constructor for the Reporter class.

        Args:
            square_size (float): the size of the square
            teamName: (str): the name of the associated team
        """
        super().__init__("reporter", "reporter", pygame.image.load("./src/djambi/assets/reporter.png"), teamName)
        self.icon = pygame.transform.scale(self.icon, (square_size, square_size))
        self.current_position = None

    def occupy_labyrinth(self, labyrinth_position):
        """check if the Reporter occupies the labyrinth

        Args:
            labyrinth_position (float): the position of the labyrinth
        """
        if self.current_position == labyrinth_position:
            # TODO: Implement the power of the Reporter, can kill an adjacent enemy piece
            self.use_power()
        pass

    def use_power(self, target_piece: Piece):
        """use the power of the Reporter to kill an adjacent enemy piece

        Args:
            target_piece (Piece): the piece to kill
        """
        if not self.controlled_by_enemy and self.is_adjacent(target_piece):
            self.kill(target_piece)
        pass

    def kill(self, target_piece: Piece):
        """_summary_

        Args:
            target_piece (Piece): kill the target piece
        """
        target_piece.die()

    def is_adjacent(self, target_piece: Piece):
        """Check if the target piece is adjacent to the reporter on col and row

        Args:
            target_piece (Piece): piece adjacent to the reporter

        Returns:
            bool: True if the target piece is adjacent to the reporter, False otherwise
        """
        # TODO : Implement the is_adjacent method
        if self.current_position[0] == target_piece.current_position[0] and abs(self.current_position[1] - target_piece.current_position[1]) == 1:
            return True
        pass

    def controlled_by(self, enemy: Team):
        """Case where the reporter is controlled by the enemy

        Args:
            enemy (Team): the team that controls the reporter
        """
        self.controlled_by_enemy = enemy
