from djambi.classes.pieces.piece import Piece
import pygame

'''Method to create a Chef piece'''


class Chef(Piece):
    def __init__(self, square_size, teamName):
        super().__init__("Chef", "Chef", pygame.image.load("./src/djambi/assets/chef.png"), teamName)
        """__init__ Constructor for the Chef class.

        Args:
            square_size (float): the size of the square
            teamName (str): the name of the associated team
        """
        self.icon = pygame.transform.scale(self.icon, (square_size, square_size))
        self.current_position = None

    def occupy_labyrinth(self, labyrinth_position):
        """check if the Chef occupies the labyrinth

        Args:
            labyrinth_position (float): the position of the labyrinth
        """
        if self.current_position == labyrinth_position:
            self.use_power()

    def use_power(self, player_turn: int, number_of_players: int):
        """Activate the power of the Chef, give an extra turn after every player turn

        Args:
            player_turn (int): the current player's turn
            number_of_players (int): the number of players
            
        Returns:
            int: the next player's turn
        """
        # Subtract 1 from player_turn to match 0-indexing
        player_turn -= 1

        # If it's the last player's turn, return the Chef's turn
        if player_turn == number_of_players - 1:
            return 0  # Assuming the Chef is the first player
        else:
            # Otherwise, return the next player's turn
            return player_turn + 1
        
            
    def die(self):
        """Kill the Chef piece
        """
        self.is_alive = False
