from djambi.classes.pieces.piece import Piece
from djambi.classes.pieces.chef import Chef
import pygame

''' Method to create a Militant piece '''


class Militant(Piece):
    def __init__(self, square_size, teamName):
        """__init__ Constructor for the Militant class.

        Args:
            square_size (float): the size of the square
            teamName (str): the name of the associated team
        """
        super().__init__("Militant", "Militant", pygame.image.load("./src/djambi/assets/militant.png"), teamName)
        self.icon = pygame.transform.scale(self.icon, (square_size, square_size))
        self.current_position = None

    def kill(self, target_piece: Piece, free_positions):
        """Method that defines what happened when the militant kills a piece

        Args:
            target_piece (Piece): the piece to kill
            free_positions (float): the free positions on the board

        Returns:
            Piece : the pieced killed
        """
        if isinstance(target_piece, Chef):
            return  # The militant cannot kill a Chef
        target_piece.die()
        target_piece.current_position = None
        # TODO: Implement this method to return the current position of the piece to kill

    def occupy_labyrinth(self, labyrinth_position):
        """Method that defines the behavior of the militant when it occupies the labyrinth

        Args:
            labyrinth_position (float): the position of the labyrinth
        """
        if self.current_position == labyrinth_position:
            self.power_active = True

    def die(self):
        """Method that kill the Militant
        """
        self.is_alive = False
