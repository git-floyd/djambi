from djambi.classes.pieces.chef import Chef
from djambi.classes.pieces.piece import Piece
import pygame

'''This module contains the Assasin class.
The Assassin is a piece that can kill the Chef piece if it is on the same case.
The Assassin can only use its power once per game.'''


class Assasin(Piece):
    def __init__(self, square_size, teamName):
        """__init__ Constructor for the Assasin class.

        Args:
            square_size (float): the size of the square
            teamName (str): the team name associated with this pawn
        """
        super().__init__("assassin", "assassin", pygame.image.load("./src/djambi/assets/assassin.png"), teamName)
        self.icon = pygame.transform.scale(self.icon, (square_size, square_size))
        self.current_position = None
        self.power_active = False
        self.previous_position = None

    def kill(self, target_piece: Piece):
        """This method kills the target piece and moves it back to its previous position. It is used when the Assassin uses its power.

        Args:
            target_piece (Piece): the piece to kill
        """
        target_piece.die()
        target_piece.current_position = self.previous_position

    def occupy_labyrinth(self, labyrinth_position):
        """This method check if the Assassin is on the same case as the Chef.
        If it is, the Assassin can use its power. The power is only available once per game.

        Args:
            labyrinth_position (float): the position of the labyrinth
        """
        if self.current_position == labyrinth_position:
            self.power_active = True

    def use_power(self, player_turn, chef_piece: Chef):
        """This method moves the Assassin to the new position and saves the previous position.

        Args:
            player_turn (int): the current player's turn
            chef_piece (Chef): the Chef piece

        Returns:
            bool: True if the Assassin gets an extra turn, False otherwise
        """
        if self.power_active and self.current_position == chef_piece.current_position:
            self.kill(chef_piece)
            self.power_active = False
            return True  # Return True to indicate that the Assassin gets an extra turn
        return False  # Return False to indicate that the turn should pass to the next player

    def die(self):
        """kill the Assassin piece
        """
        self.is_alive = False
