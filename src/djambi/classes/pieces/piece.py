'''The class Piece represents a piece in the Djambi game. Its an Interface. It is responsible for creating the pieces of the game.'''


class Piece:
    def __init__(self, type, name, icon, teamName):
        """__init__ Constructor for the Piece interface.

        Args:
            type (string): The type of the piece
            name (string): The name of the piece
            icon (sring): url that contains the icon of the piece
        """
        self.type = type
        self.name = name
        self.icon = icon
        self.teamName = teamName
        self.is_dead = False
