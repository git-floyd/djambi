from djambi.classes.pieces.piece import Piece
import pygame

'''Method to create a Necromobile piece'''


class Necromobile(Piece):
    def __init__(self, square_size, teamName):
        """__init__ Constructor for the Assasin class.

        Args:
            square_size (float): the size of the square
            teamName (str): the name of the associated team
        """
        super().__init__("necromobile", "necromobile", pygame.image.load("./src/djambi/assets/necromobile.png"), teamName)
        self.icon = pygame.transform.scale(self.icon, (square_size, square_size))
        self.current_position = None
        self.is_alive = True

    def occupy_labyrinth(self, labyrinth_position):
        """Method that descibe the behavior of the Necromobile when it occupies the labyrinth

        Args:
            labyrinth_position (float): the position of the labyrinth
        """
        if self.current_position == labyrinth_position:
            self.power_active = True

    def use_power(self, target_piece: Piece, move_position):
        """The function to use the power of the Necromobile

        Args:
            player_turn (int): the current player's turn
            target_piece (Piece): the piece to move
            move_position (float): the position where the ennemy pice will be moved

        Returns:
            bool: return a bool to indicate if the power is used or not, True if the Necromobile gets an extra turn, False otherwise
        """
        if self.power_active:
            self.power_active = False
            # TODO : Implement this line to move the ennemy piece to a free position
            self.move_ennemy(target_piece, move_position)
            return True  # Return True to indicate that the Necromobile gets an extra turn
        return False  # Return False to indicate that the turn should pass to the next player

    def move_ennemy(self, target_piece: Piece, move_position):
        """Move the ennemy piece to a free position
Args:
            target_piece (Piece): the piece to move
            move_position (float): the position where the ennemy pice will be moved
        """
        if self.occupy_labyrinth():
            target_piece.die()
            target_piece.current_position = move_position

    def die(self):
        """Kill the Necromobile piece
        """
        self.is_alive = False
