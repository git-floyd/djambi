from djambi.classes.pieces.piece import Piece
import pygame

'''Method to create a Diplomate piece'''


class Diplomate(Piece):
    def __init__(self, square_size, teamName):
        """__init__ Constructor for the Diplomate class.

        Args:
            square_size (float): the size of the square
            teamName (str): the name of the associated team
        """
        super().__init__("Diplomate", "Diplomate", pygame.image.load("./src/djambi/assets/diplomate.png"), teamName)
        self.icon = pygame.transform.scale(self.icon, (square_size, square_size))
        self.current_position = None
        self.power_active = False

    def is_on_ennemy(self, target_piece: Piece):
        """Method that use power if Diplomate occupies the labyrinth

        Args:
            target_piece (Piece): the piece to move
        """
        if self.current_position == target_piece.current_position:
            self.move_ennemy(target_piece)

    def move_ennemy(self, target_piece: Piece, move_position):
        """Method that specifies the movement for the targeted piece when Diplomate uses its power

        Args:
            target_piece (Piece): the piece to move
            move_position (float): the position where the ennemy piece will be moved
        """
        if self.is_on_ennemy(target_piece):
            target_piece.current_position = move_position
        # TODO: Implement this method to move the ennemy piece anywhere on the board

    def die(self):
        """Method that kill the Diplomate
        """
        self.is_alive = False
