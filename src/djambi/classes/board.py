from djambi.classes.case import Case
from djambi.setup.constants import BLACK, TR, TL, BR, BL, COLORS, Roles
from djambi.classes.team import Team
import pygame

''' This module contains the Board class. It represents the board of the Djambi game.
The board is composed of cases. The board is responsible for managing the cases of the board.'''


class Board:
    def __init__(self, rows, cols, screen_width, screen_height, number_of_teams):
        """__init__ Constructor for the Board class.

        Args:
            rows (int): The number of rows of the board
            cols (int): The number of columns of the board
            screen_width (int): The width of the screen in pixels
            screen_height (int): The height of the screen in pixels
        """
        self.rows = rows
        self.cols = cols
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.number_of_teams = number_of_teams
        self.teams = []
        self.board = []

    def build_board(self):
        """Method that builds the board. It is called to create the board. It allows to create the cases of the board.
        """
        for row in range(self.rows):
            self.board.append([])
            for col in range(self.cols):
                self.board[row].append(Case(row, col))
        self.init_teams()

    def draw(self, screen):
        """draw white line for create case in board

        Args:
            screen (Object): Screen variable of the game in Game.py file. Needed from pygame
        """
        # Draw pieces on the board
        square_width = self.screen_width / self.cols
        square_height = self.screen_height / self.rows

        for row in range(self.rows):
            for col in range(self.cols):
                # Check if the row and column indices are within the range of the board
                if row < len(self.board) and col < len(self.board[row]):
                    pygame.draw.rect(screen, BLACK, (
                        col * self.screen_width / self.cols, row * self.screen_height / self.rows,
                        self.screen_width / self.cols, self.screen_height / self.rows), 1)
                    case = self.board[row][col]
                    if case.is_occupied():
                        piece = case.piece
                        team_color = pygame.Color(COLORS[piece.teamName])
                        pygame.draw.rect(screen, team_color,
                                         (col * square_width, row * square_height, square_width, square_height)
                                         )

                        icon = piece.icon
                        # Redimensionner l'icône à la taille de la case
                        icon = pygame.transform.scale(icon, (int(square_width * 0.8), int(square_height * 0.8)))
                        x = col * square_width + (square_width - icon.get_width()) / 2
                        y = row * square_height + (square_height - icon.get_height()) / 2
                        screen.blit(icon, (x, y))

    def init_teams(self):
        for team_index, team_color in enumerate(COLORS):
            self.teams.append(Team(team_color, f"Team {team_index + 1}"))

        # Place pieces on the board
        for row in range(self.rows):
            for col in range(self.cols):
                if row <= 3 and col <= 3:  # Top left corner
                    self.place_pieces(self.board[row][col], TL, 0)
                elif row <= 3 and col >= 6:  # Top right corner
                    self.place_pieces(self.board[row][col], TR, 1)
                elif row >= 6 and col <= 3:  # Bottom left corner
                    self.place_pieces(self.board[row][col], BL, 2)
                elif row >= 6 and col >= 6:  # Bottom right corner
                    self.place_pieces(self.board[row][col], BR, 3)

    def place_pieces(self, case, positions, team_index):
        case_x, case_y = case.position
        if (case_x, case_y) in positions["militant"]:
            piece = Roles.Militant.value(50, team_index)
        elif "necromobile" in positions and (case_x, case_y) == positions["necromobile"]:
            piece = Roles.Necromobile.value(50, team_index)
        elif (case_x, case_y) == positions["chef"]:
            piece = Roles.Chef.value(50, team_index)
        elif (case_x, case_y) == positions["assasin"]:
            piece = Roles.Assasin.value(50, team_index)
        elif (case_x, case_y) == positions["reporter"]:
            piece = Roles.Reporter.value(50, team_index)
        elif (case_x, case_y) == positions["diplomate"]:
            piece = Roles.Diplomate.value(50, team_index)
        else:
            piece = None

        if piece:
            case.set_piece(piece)

    def move_piece(self, case, target_case):
        if case.piece is not None:
            # Place the piece on the target case
            target_case.set_piece(case.piece)
            # Remove the piece from its current position
            case.set_piece(None)
