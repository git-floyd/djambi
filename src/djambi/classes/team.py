from djambi.classes.pieces.assasin import Assasin
from djambi.classes.pieces.chef import Chef
from djambi.classes.pieces.diplomate import Diplomate
from djambi.classes.pieces.militant import Militant
from djambi.classes.pieces.necromobile import Necromobile
from djambi.classes.pieces.reporter import Reporter


class Team:
    def __init__(self, color, name):
        self.color = color
        self.name = name
        self.is_possessed_by = None
        self.is_asleep = False
        self.pieces = []

    def get_color(self):
        return self.color

    def name(self):
        return self.name

    def create_pieces(self, pices_set):
        for pieces in pices_set:
            for row in range(3):
                for col in range(3):
                    for piece in pieces:
                        if piece == "chef":
                            self.pieces.append(Chef(50, self.name))
                        if piece == "militant":
                            for militant in piece:
                                self.pieces.append(Militant(50, self.name))
                        if piece == "diplomate":
                            self.pieces.append(Diplomate(50, self.name))
                        if piece == "assasin":
                            self.pieces.append(Assasin(50, self.name))
                        if piece == "reporter":
                            self.pieces.append(Reporter(50, self.name))
                        if piece == "necromobile":
                            self.pieces.append(Necromobile(50, self.name))

    def get_pieces(self):
        return self.pieces
