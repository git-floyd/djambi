import pygame
from djambi.game import Game

'''This module contains the main function of the Djambi game. It is the entry point of the game.'''
pygame.init()
screen = pygame.display.set_mode((1080, 920))
pygame.display.set_caption("Djambi - Git Floyd")

'''Create a Game instance and run the game.'''
game_instance = Game(screen)
game_instance.run()

'''Quit the game.'''
pygame.quit()
