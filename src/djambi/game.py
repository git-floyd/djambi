import pygame
from djambi.pages.board_view import BoardView

'''This module contains the Game class. It represents the Djambi game. The game is composed of a board and a board view.
The game is responsible for managing the game loop and the game elements.'''


class Game:
    def __init__(self, screen):
        """__init__ Constructor for the Game class.

        Args:
            screen (Object): the screen of the game
        """
        self.screen = screen
        self.clock = pygame.time.Clock()
        self.running = True  # Initialize running attribute
        self.board_view = BoardView(screen)
        self.prev_selected_case = None
        self.selected_case = None

    # Method for managing events
    def handling_events(self):
        """Method that manages the events of the game. It is called at each iteration of the game loop. It allows to manage the events of the game.
        """
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:  # Clic gauche
                    mouse_x, mouse_y = pygame.mouse.get_pos()
                    square_width = self.screen.get_width() / self.board_view.board.cols
                    square_height = self.screen.get_height() / self.board_view.board.rows
                    col = int(mouse_x // square_width)
                    row = int(mouse_y // square_height)
                    if self.prev_selected_case:
                        self.selected_case = self.board_view.board.board[row][col]
                        if self.selected_case != self.prev_selected_case:
                            self.board_view.board.move_piece(self.prev_selected_case, self.selected_case)
                            self.prev_selected_case = None
                        else:
                            print("Déplacement invalide")
                    else:
                        case = self.board_view.board.board[row][col]
                        if case.is_occupied():
                            self.prev_selected_case = case

    # Method that manages logic and every update of game elements
    def update(self):
        """Method that updates the game. It is called at each iteration of the game loop. It allows to update the game elements.
        """
        pass

    def init_display(self):
        """Method that initializes the display of the game. It is called to initialize the game display. It allows to initialize the game elements.
        """
        self.board_view.first_draw()

    # Display management method
    def display(self):
        """Method that displays the game. It is called at each iteration of the game loop. It allows to display the game elements.
        """
        self.board_view.draw()

    # Method for managing game startup
    def run(self):
        """Method that runs the game loop. It is called to start the game. It manages the game loop.
        """
        self.init_display()
        while self.running:
            self.handling_events()  # Call event handling method
            self.update()  # Call update method
            self.display()  # Call display method
            pygame.display.flip()  # Update the display
            self.clock.tick(30)  # Number of frames per second
