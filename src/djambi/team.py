from djambi.classes.pieces.piece import Piece

'''This module contains the Team class. It represents a team in the Djambi game. A team take a color and a name as parameters.'''


class Team:
    def __init__(self, color, name):
        """__init__ Constructor for the Team class.

        Args:
            color (string): the color of the team
            name (string): the team name
        """
        self.color = color
        self.name = name
        self.is_possessed_by = None
        self.is_asleep = False

    @property
    def color(self):
        """Returns the color of the team as a string. By calling t.color, you can get the color of the team t.

        Returns:
            string: the color of the team
        """
        return self.color

    @property
    def name(self):
        """Returns the name of the team as a string. By calling t.name, you can get the name of the team t.

        Returns:
            sring: the name of the team
        """
        return self.team_names

    def add_piece(self, piece: Piece):
        """Returns the team that possesses the team. Is used to know which team is playing.

        Args:
            piece (Piece): the piece to add to the team

        Returns:
            Array : an array of pieces contained in the team
        """
        self.pieces.append(piece)
        return self

    def get_pieces(self, piece: Piece):
        """Return the pieces of the team as a list.

        Args:
            piece (Piece): a piece to get in the team

        Returns:
            piece (Piece): the piece to get in the team
        """
        return self.pieces
