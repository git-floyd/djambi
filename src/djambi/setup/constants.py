from enum import Enum
from djambi.classes.pieces.chef import Chef
from djambi.classes.pieces.militant import Militant
from djambi.classes.pieces.necromobile import Necromobile
from djambi.classes.pieces.diplomate import Diplomate
from djambi.classes.pieces.reporter import Reporter
from djambi.classes.pieces.assasin import Assasin

WIDTH_DEFAULT, HEIGHT_DEFAULT = 1080, 920
FPS = 60
ROWS, COLS = 9, 9

# RGB
RED = (255, 0, 0)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
GREEN = (0, 255, 0)

# Assets
BORDER_SIZE = 1

COLORS = ['red', 'blue', 'green', 'yellow']
TL = {
    "chef": (0, 0),
    "militant": [(2, 0), (2, 1), (0, 2), (1, 2)],
    "necromobile": (2, 2),
    "assasin": (0, 1),
    "reporter": (1, 0),
    "diplomate": (1, 1)
}
TR = {
    "chef": (0, 8),
    "militant": [(2, 8), (2, 7), (0, 6), (1, 6)],
    "necromobile": (2, 6),
    "assasin": (0, 7),
    "reporter": (1, 8),
    "diplomate": (1, 7)
}
BL = {
    "chef": (8, 0),
    "militant": [(6, 0), (6, 1), (8, 2), (7, 2)],
    "necromobile": (6, 2),
    "assasin": (8, 1),
    "reporter": (7, 0),
    "diplomate": (7, 1)
}
BR = {
    "chef": (8, 8),
    "militant": [(6, 8), (6, 7), (8, 6), (7, 6)],
    "necromobile": (6, 6),
    "assasin": (8, 7),
    "reporter": (7, 8),
    "diplomate": (7, 7)
}


class Roles(Enum):
    Chef = Chef
    Militant = Militant
    Diplomate = Diplomate
    Necromobile = Necromobile
    Assasin = Assasin
    Reporter = Reporter


CELL_SIZE = 50
